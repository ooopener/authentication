<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/oauth/revoke' ,
    [ $container->revokeController , 'revoke' ]
)
->setName( 'oauth.revoke' ) ;

$application->post
(
    '/oauth/revoke' , [ $container->revokeController , 'revoke' ]
)
->setName( 'oauth.revoke.post' ) ;
