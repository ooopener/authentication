<?php

namespace com\ooopener\things ;

use JsonSerializable;

use core\Objects;

/**
 * Class TokenPayload
 * @package com\ooopener\things
 */
class TokenPayload implements JsonSerializable
{
    /**
     * TokenPayload constructor.
     */
    public function __construct()
    {
    }

    /**
     * The internal id of the token
     * @var string
     */
    public $id ;

    /**
     * A unique token identifier of the token (JWT id)
     * @var string
     */
    public $jti ;

    /**
     * The id of the server who issued the token (Issuer)
     * @var string
     */
    public $iss ;

    /**
     * The id of the client who requested the token (Audience)
     * @var string
     */
    public $aud ;

    /**
     * The id of the user for which the token was released (Subject)
     * @var string
     */
    public $sub ;

    /**
     * UNIX timestamp when the token expires (Expiration)
     * @var integer
     */
    public $exp ;

    /**
     * UNIX timestamp when the token was created (Issued At)
     * @var integer
     */
    public $iat ;

    /**
     * UNIX timestamp when the token is valid (Not Before)
     * @var integer
     */
    public $nbf ;

    /**
     * @var string
     */
    public $scope ;

    /**
     * Invoked to serialize the object with the json serializer.
     */
    public function jsonSerialize()
    {
        $object =
        [
            'id'    => $this->id,
            'jti'   => $this->jti,
            'iss'   => $this->iss,
            'aud'   => $this->aud,
            'sub'   => $this->sub,
            'exp'   => $this->exp,
            'iat'   => $this->iat,
            'nbf'   => $this->nbf,
            'scope' => $this->scope
        ];

        return Objects::compress( $object ) ;
    }
}