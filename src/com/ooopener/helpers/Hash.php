<?php

namespace com\ooopener\helpers;

/**
 * The Hash class.
 * <pre>
 * $expression = "ilovecats" ;
 * $password   = $app->hash->password($expression) ;
 *
 * var_dump( $app->hash->check($expression, $password)) ;
 * </pre>
 */
class Hash
{
    /**
     * Creates a new Hash instance.
     *
     * @param mixed $config
     */
    public function __construct( $config = NULL )
    {
        $this->config = $config ;
    }

    /**
     * The config reference of the object.
     */
    protected $config ;

    /**
     * Generates a unique identifier (uuid).
     */
    public function generateUUID()
    {
        return sprintf
        (
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,
            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Generate a secret
     */
    public function generateSecret()
    {
        return bin2hex( openssl_random_pseudo_bytes( 16 ) );
    }

    /**
     * Hash the specified expression with a Sha256 algorithm.
     *
     * @param string $input
     *
     * @return string
     */
    public function hash( $input )
    {
        return hash( 'sha256' , $input ) ;
    }

    /**
     * Checks the hash of the specified string expression.
     *
     * @param string $know
     * @param string $user
     *
     * @return bool
     */
    public function hashCheck( $know , $user  )
    {
        return hash_equals( $know , $user ) ;
    }

    /**
     * Generates a new password.
     *
     * @param string $expression
     *
     * @return bool|string
     */
    public function password( $expression )
    {
        return password_hash
        (
            $expression ,
            $this->config['hash']['algo'] ,
            [
                $this->config['hash']['cost']
            ]
        ) ;
    }

    /**
     * Check the specified password expression.
     *
     * @param string $expression
     * @param string $hash
     *
     * @return bool
     */
    public function passwordCheck( $expression , $hash )
    {
        return password_verify( $expression , $hash ) ;
    }

    /**
    * Returns a String representation of the object.
    * @return string A string representation of the object.
    */
    public function __toString() /*String*/
    {
        return '[' . get_class($this) . ']' ;
    }
}

