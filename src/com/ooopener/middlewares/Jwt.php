<?php

namespace com\ooopener\middlewares;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\Container ;

use Slim\Middleware\JwtAuthentication;

class Jwt extends JwtAuthentication
{
    private $container;

    /**
     * Jwt constructor.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        $this->container = $container;

        $algorithm   = $container->settings['token']['algorithm'] ;
        $cookieName  = $container->settings['token']['cookie_name'];
        $path        = $container->settings['token']['path'];
        $passthrough = $container->settings['token']['passthrough'];
        $secure      = $container->settings['token']['secure'];
        $secret      = $container->settings['token']['key'];

        $relaxed      = $container->settings['jwt']['relaxed'];

        parent::__construct(
            [
                "algorithm"   => $algorithm ,
                "secure"      => $secure ,
                "cookie"      => $cookieName ,
                "relaxed"     => $relaxed ,
                "path"        => $path ,
                "passthrough" => $passthrough ,
                "secret"      => $secret ,
                "callback"    => function (RequestInterface $request , ResponseInterface $response , $arguments)
                {
                    $jwt = $arguments["decoded"];

                    if( isset( $jwt->sub ) )
                    {
                        $_SESSION[ $this->container->settings['auth']['session'] ] = $jwt->sub ;
                    }

                    $this->container->jwt = $jwt;

                }
            ] );

        if( (bool) $this->container->settings['useLogging'] )
        {
            $this->setLogger( $this->container->logger );
        }
    }

    /**
     * Handle error
     *
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param mixed[] $arguments
     *
     * @return mixed
     */
    public function error( RequestInterface $request , ResponseInterface $response , $arguments )
    {
        $path = $request->getUri()->getPath() ;
        if( $path == "oauth/authorize" )
        {
            // save to redirect
            $fullUrl = (string)$request->getUri() ;

            $_SESSION[ "urlRedirect" ] = $fullUrl ;

            // go to login
            return $this->container['response']->withRedirect( $this->container->router->pathFor( "api.login" ) ) ;

        }

        $format = $this->container['format'] ;

        switch( $format )
        {
            case 'json' :
                $data["status"] = "error JWT";
                $data["message"] = $arguments["message"];

                return $response->withHeader("Content-Type", "application/json")->write(json_encode($data, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
                break ;
            default :
                // save to redirect
                $fullUrl = (string)$request->getUri() ;

                if( $path == "/" )
                {
                    $fullUrl = substr( $fullUrl , 0 , -1 ) ;
                }
                $_SESSION[ "urlRedirect" ] = $fullUrl ;

                // go to login
                return $this->container['response']->withRedirect( $this->container->router->pathFor( "api.login" ) ) ;
                break ;
        }
    }

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return '[' . get_class($this) . ']' ;
    }
}


