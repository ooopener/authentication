<?php

use Slim\Http\Response ;

use com\ooopener\helpers\token\GrantType ;

use com\ooopener\models\Applications ;
use com\ooopener\models\AuthorizationCodes ;
use com\ooopener\models\Tokens ;

class TokenControllerTest extends SlimAppTest
{
    public $applications ;

    public $authorizationCodes ;

    public $tokens ;

    public function setUp()
    {
        parent::setUp();

        $this->applications       = $this->createMock( Applications::class ) ;
        $this->authorizationCodes = $this->createMock( AuthorizationCodes::class ) ;
        $this->tokens             = $this->createMock( Tokens::class ) ;
    }

    public function testTokenEmpty()
    {
        // GET
        $request = $this->getRequest( 'GET' , '/oauth/token' ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/token' ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testTokenAuthorizationCodeMissingParams()
    {
        $data_requests =
        [
            'empty' =>
            [
                'grant_type' => GrantType::AUTHORIZATION_CODE
            ],
            'withoutClientSecret' =>
            [
                'grant_type' => GrantType::AUTHORIZATION_CODE ,
                'client_id'  => 'testID'
            ],
            'withoutCode' =>
            [
                'grant_type'    => GrantType::AUTHORIZATION_CODE ,
                'client_id'     => 'testID',
                'client_secret' => 'clientSecret'
            ]
        ];

        $methods = [ 'GET' , 'POST' ] ;

        foreach( $data_requests as $data_request )
        {
            foreach( $methods as $method )
            {
                if( $method === 'GET' )
                {
                    $request = $this->getRequest( $method , '/oauth/token' , $data_request ) ;
                }
                else
                {
                    $request = $this->getRequest( $method , '/oauth/token' , null , $data_request ) ;
                }

                $response = new Response() ;
                $response = $this->container->tokenController->token( $request , $response ) ;

                $this->assertEquals( 400 , $response->getStatusCode() ) ;
            }
        }
    }

    public function testTokenCredentialsMissingParams()
    {
        $data_requests =
        [
            'empty' =>
            [
                'grant_type' => GrantType::CLIENT_CREDENTIALS
            ],
            'withoutClientSecret' =>
            [
                'grant_type' => GrantType::CLIENT_CREDENTIALS ,
                'client_id'  => 'testID'
            ]
        ];

        $methods = [ 'GET' , 'POST' ] ;

        foreach( $data_requests as $data_request )
        {
            foreach( $methods as $method )
            {
                if( $method === 'GET' )
                {
                    $request = $this->getRequest( $method , '/oauth/token' , $data_request ) ;
                }
                else
                {
                    $request = $this->getRequest( $method , '/oauth/token' , null , $data_request ) ;
                }

                $response = new Response() ;
                $response = $this->container->tokenController->token( $request , $response ) ;

                $this->assertEquals( 400 , $response->getStatusCode() ) ;
            }
        }
    }

    public function testTokenRefreshMissingParams()
    {
        $data_requests =
        [
            'empty' =>
            [
                'grant_type' => GrantType::REFRESH_TOKEN
            ],
            'withoutClientSecret' =>
            [
                'grant_type' => GrantType::REFRESH_TOKEN ,
                'client_id'  => 'testID'
            ],
            'withoutRefresh' =>
            [
                'grant_type'    => GrantType::REFRESH_TOKEN ,
                'client_id'     => 'testID',
                'client_secret' => 'clientSecret'
            ]
        ];

        $methods = [ 'GET' , 'POST' ] ;

        foreach( $data_requests as $data_request )
        {
            foreach( $methods as $method )
            {
                if( $method === 'GET' )
                {
                    $request = $this->getRequest( $method , '/oauth/token' , $data_request ) ;
                }
                else
                {
                    $request = $this->getRequest( $method , '/oauth/token' , null , $data_request ) ;
                }

                $response = new Response() ;
                $response = $this->container->tokenController->token( $request , $response ) ;

                $this->assertEquals( 400 , $response->getStatusCode() ) ;
            }
        }
    }

    public function testTokenInvalidAuthorizationCode()
    {
        // fake db result

        $this->authorizationCodes->method( 'check' )
            ->will( $this->returnValue( false ) ) ;

        $this->container['authorizationCodes'] = $this->authorizationCodes ;

        $data_request =
        [
            'grant_type'    => GrantType::AUTHORIZATION_CODE ,
            'client_id'     => 'testID' ,
            'client_secret' => 'clientSecret' ,
            'code'          => 'testCode'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/token' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/token' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testTokenInvalidRefreshToken()
    {
        // fake db result

        $this->tokens->method( 'checkRefresh' )
            ->will( $this->returnValue( false ) ) ;

        $this->container->tokens = $this->tokens ;

        $data_request =
        [
            'grant_type'    => GrantType::REFRESH_TOKEN ,
            'client_id'     => 'testID' ,
            'client_secret' => 'clientSecret' ,
            'refresh_token' => 'testRefreshToken'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/token' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/token' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testTokenInvalidApplication()
    {
        // fake db result

        $this->applications->method( 'getByProperty' )
            ->will( $this->returnValue( false ) ) ;

        $this->container['applications'] = $this->applications ;

        $data_request =
        [
            'grant_type'    => GrantType::CLIENT_CREDENTIALS ,
            'client_id'     => 'testID' ,
            'client_secret' => 'clientSecret'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/token' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 401 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/token' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 401 , $response->getStatusCode() ) ;
    }

    public function testTokenValidAuthorizationCode()
    {
        // fake db result
        $rowApplicationDB = (object)
        [
            'client_secret' => 'clientSecret'
        ];

        $this->applications->method( 'getByProperty' )
            ->will( $this->returnValue( $rowApplicationDB ) ) ;

        $this->authorizationCodes->method( 'check' )
            ->will( $this->returnValue( true ) ) ;

        $this->tokens->method( 'insert' )
            ->will( $this->returnValue( true ) ) ;

        $this->container['applications'] = $this->applications ;
        $this->container['authorizationCodes'] = $this->authorizationCodes ;
        $this->container->tokens = $this->tokens ;

        $data_request =
        [
            'grant_type'    => GrantType::AUTHORIZATION_CODE ,
            'client_id'     => 'testID' ,
            'client_secret' => 'clientSecret' ,
            'code'          => 'testCode'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/token' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/token' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
    }

    public function testTokenValidClientCredentials()
    {
        // fake db result
        $rowApplicationDB = (object)
        [
            'client_secret' => 'clientSecret'
        ];

        $this->applications->method( 'getByProperty' )
            ->will( $this->returnValue( $rowApplicationDB ) ) ;

        $this->tokens->method( 'insert' )
            ->will( $this->returnValue( true ) ) ;

        $this->container['applications'] = $this->applications ;
        $this->container->tokens = $this->tokens ;

        $data_request =
        [
            'grant_type'    => GrantType::CLIENT_CREDENTIALS ,
            'client_id'     => 'testID' ,
            'client_secret' => 'clientSecret'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/token' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/token' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
    }

    public function testTokenValidRefreshToken()
    {
        // fake db result
        $rowApplicationDB = (object)
        [
            'client_secret' => 'clientSecret'
        ];

        $rowTokenDB = (object)
        [
            'ip'         => '127.0.0.1' ,
            'user_agent' => 'Slim Framework'
        ];

        $this->applications->method( 'getByProperty' )
            ->will( $this->returnValue( $rowApplicationDB ) ) ;

        $this->tokens->method( 'checkRefresh' )
            ->will( $this->returnValue( true ) ) ;

        $this->tokens->method( 'getByProperty' )
            ->will( $this->returnValue( $rowTokenDB ) ) ;

        $this->tokens->method( 'updateToken' )
            ->will( $this->returnValue( true ) ) ;

        $this->container['applications'] = $this->applications ;
        $this->container->tokens = $this->tokens ;

        $data_request =
        [
            'grant_type'    => GrantType::REFRESH_TOKEN ,
            'client_id'     => 'testID' ,
            'client_secret' => 'clientSecret' ,
            'refresh_token' => 'testRefreshToken'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/token' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/token' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->tokenController->token( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
    }
}