<?php

namespace com\ooopener\controllers\login;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use com\ooopener\controllers\Controller ;
use com\ooopener\things\Session;
use com\ooopener\things\TokenPayload;

use Firebase\JWT\JWT;
use Firebase\Auth\Token\Exception\ExpiredToken ;
use Firebase\Auth\Token\Exception\IssuedInTheFuture;
use Firebase\Auth\Token\Exception\InvalidToken;

use Slim\Container;

use DateTime;
use DateTimeZone;

/**
 * The login controller.
 */
class LoginController extends Controller
{
    /**
     * Creates a new LoginController instance.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct( $container );
    }

    /**
     * Get login
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function get( Request $request = NULL, Response $response = NULL , array $args = [] )
    {

        // check if already logged
        $cookie = $this->container->cookie->get( $request , $this->container->settings['token']['cookie_name'] ) ;

        if( $cookie )
        {
            return $this->pathFor( $response , 'api.index' ) ;
        }

        $settings = $this->container->firebaseConfig;

        $response = $this->container->cache->denyCache( $response ) ;

        return $this->render
        (
            $response ,
            'login/login.twig' ,
            [
                'apiKey'            => $settings['apiKey'],
                'authDomain'        => $settings['authDomain'],
                'databaseURL'       => $settings['databaseURL'],
                'storageBucket'     => $settings['storageBucket'],
                'messagingSenderId' => $settings['messagingSenderId']
            ]
        ) ;
    }

    /**
     * Register user and/or create id token
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @throws
     * @return Response
     */
    public function post( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $container = $this->container ;

        $params = $request->getParsedBody() ;

        if( !isset( $params['token'] ) || !isset( $params['provider'] ) )
        {
            return $this->formatError( $response ,"400", NULL  , NULL , 400 ) ;
        }

        $firebaseToken = $params['token'] ;
        $provider      = json_decode( $params['provider'] ) ;

        // check user identity with firebase

        try
        {

            $verifiedIdToken = $container->firebase->verifyIdToken( $firebaseToken ) ;

            $uid           = $verifiedIdToken->getClaim('sub') ;
            if( $verifiedIdToken->hasClaim( 'name' ) )
            {
                $name          = $verifiedIdToken->getClaim('name') ;
            }
            else if( $provider && property_exists( $provider , 'displayName' ) )
            {
                $name = $provider->displayName ;
            }
            else
            {
                $name = '' ;
            }

            if( $verifiedIdToken->hasClaim( 'picture' ) )
            {
                $picture       = $verifiedIdToken->getClaim('picture') ;
            }
            else
            {
                $picture = null ;
            }
            $email         = $verifiedIdToken->getClaim('email') ;
            $emailVerified = $verifiedIdToken->getClaim('email_verified') ;
        }
        catch( ExpiredToken $e )
        {
            return $this->formatError( $response ,"401", NULL  , NULL , 401 ) ;
        }
        catch( IssuedInTheFuture $e )
        {
            return $this->formatError( $response ,"401", NULL  , NULL , 401 ) ;
        }
        catch( InvalidToken $e )
        {
            return $this->formatError( $response ,"401", NULL  , NULL , 401 ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response ,"401", NULL  , NULL , 401 ) ;
        }

        if( $provider->providerId != 'password' || $emailVerified == true )
        {
            try
            {
                $name = explode( ' ' , $name ) ;

                $givenName  = $name[0] ;
                $familyName = '' ;

                if( count( $name ) > 1 )
                {
                    // handle family name
                    array_shift( $name );
                    $name = implode( " " , $name );
                    $familyName = $name ;
                }

                $init =
                [
                    'givenName'    => $givenName ,
                    'familyName'   => $familyName ,
                    'email'        => $email ,
                    'uuid'         => $uid ,
                    'provider'     => $provider->providerId,
                    'provider_uid' => $provider->uid,
                    'active'       => 1
                ];

                if( $picture )
                {
                    $init['image'] = $picture ;
                }

                if( !$container->users->exist( $uid , [ 'key' => 'uuid' ] ) )
                {
                    $invitation = null ;
                    $invitation_used = false ;
                    $invitation_person = false ;

                    if( isset( $_SESSION['invitation'] ) )
                    {
                        $invitation = $_SESSION['invitation'] ;

                        if( $invitation )
                        {
                            if( property_exists( $invitation , 'team' ) )
                            {
                                $team = $container->teams->get( $invitation->team ) ;
                                if( $team && $team->name )
                                {
                                    $init['team'] = $team->name ;
                                    $invitation_used = true ;
                                }
                            }

                            if( property_exists( $invitation , 'person' ) && $container->people->exist( $invitation->person ) )
                            {
                                $init['person'] = $invitation->person ;
                                $invitation_person = true ;
                            }

                        }
                    }

                    $id = $container->users->insert( $init ) ;

                    if( $invitation && property_exists( $invitation , 'person' ) )
                    {
                        $up =
                        [
                            'user'       => $uid ,
                            'module'     => 'people' ,
                            'resource'   => $invitation->person ,
                            'permission' => 'W'
                        ];

                        // add user permission for the specific person
                        $container->userPermissions->insert( $up ) ;
                        $invitation_used = true ;
                    }

                    if( $invitation && $invitation_used )
                    {
                        $container->invitationCodes->used( $invitation->id ) ;
                    }
                }

                if( isset( $_SESSION['invitation'] ) )
                {
                    unset( $_SESSION['invitation'] ) ;
                }

                if( $uid )
                {
                    $_SESSION[ $container->settings['auth']['session'] ] = $uid ;

                    // get team/user permissions
                    $user = $this->container->usersController->getUuid( NULL , NULL , [ 'id' => $uid , 'skin' => 'full' ] ) ;

                    $scope = $user->scope ;

                    # create token

                    $now = new DateTime();
                    $future = new DateTime("now +" . $this->container->settings['token']['live_id_token'] . " seconds");

                    $tokenUid = $container->hash->generateUUID() ;

                    $payload = new TokenPayload();

                    $payload->jti = $tokenUid ;
                    $payload->iss = $this->container->settings['app']['url'] ;
                    $payload->sub = $user->uuid ;
                    $payload->exp = $future->getTimestamp() ;
                    $payload->iat = $now->getTimeStamp() ;
                    $payload->scope = json_encode( $scope );

                    $tokenJWT = JWT::encode( $payload , $container->settings['token']['key'] , $container->settings['token']['algorithm'][0] );

                    # set cookie
                    $response = $container->cookie->set
                    (
                        $response ,
                        $container->settings['token']['cookie_name'] ,
                        $tokenJWT ,
                        $future ,
                        '/' . $container->settings['version']
                    );

                    $future->setTimezone( new DateTimeZone( "Etc/Zulu" ) );

                    // save session infos
                    $session = new Session();

                    $session->type       = "id_token" ;
                    $session->user       = $user->uuid ;
                    $session->token_id   = $tokenUid ;
                    $session->ip         = $container->userInfos->getIp() ;
                    $session->agent      = $container->userInfos->getUserAgent() ;
                    $session->expired    = $future->format( "Y-m-d\TH:i:s.v\Z" ) ;

                    $container->sessions->insert( $session );

                    $args = [];

                    if ( array_key_exists( 'urlRedirect' , $_SESSION ) )
                    {
                        $args["redirect"] = $_SESSION['urlRedirect'];
                        unset( $_SESSION['urlRedirect'] );
                    }

                    return $this->success( $response , "ok" , '' , NULL , $args );
                }
            }
            catch( Exception $e )
            {
                return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ]  , NULL , 500 );
            }
        }
        else
        {
            $this->logger->debug( $this . ' email not verified!' ) ;

            return $this->formatError( $response ,"401", NULL  , NULL , 401 );
        }

    }
}


