<?php

namespace com\ooopener\things;

/**
 * An authorization code.
 */
class AuthorizationCode
{
    /**
     * Creates a new AuthorizationCode instance.
     * @param object $init A generic object containing properties with which to populate the newly instance. If this argument is null, it is ignored.
     */
    public function __construct( $init = NULL )
    {
        if( isset( $init ) )
        {
            foreach ($init as $key => $value)
            {
                if( property_exists( $this , $key ) )
                {
                    $this->{ $key } = $value ;
                }
            }
        }
    }

    /**
     * The id.
     * @var integer
     */
    public $id ;

    /**
     * The user id.
     * @var string
     */
    public $user_id ;

    /**
     * The client id.
     * @var string
     */
    public $client_id ;

    /**
     * The code.
     * @var string
     */
    public $code ;

    /**
     * @var string
     */
    public $ip ;

    /**
     * Date of creation of the resource.
     */
    public $created ;

    /**
     * Date of the expiration of the resource.
     */
    public $expired ;

    /**
     * The 'date' filter constant.
     */
    const FILTER_DATE = 'date' ;

    /**
     * The default filter constant (NULL).
     */
    const FILTER_DEFAULT = NULL ;


    /**
     * The enumeration of all object properties.
     */
    public static $properties =
    [
        'id'        => self::FILTER_DEFAULT ,
        'user_id'   => self::FILTER_DEFAULT ,
        'client_id' => self::FILTER_DEFAULT ,
        'code'      => self::FILTER_DEFAULT ,
        'ip'        => self::FILTER_DEFAULT ,
        'created'   => self::FILTER_DATE ,
        'expired'   => self::FILTER_DATE
    ];

    /**
     * Returns a String representation of the object.
     * @return string A string representation of the object.
     */
    public function __toString() /*String*/
    {
        return "[" . get_class($this) . "]" ;
    }
}

