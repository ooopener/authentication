<?php

use Slim\Http\Response ;

use com\ooopener\models\Applications ;
use com\ooopener\models\AuthorizationCodes ;
use com\ooopener\models\UsersAuthApps ;

class AuthorizationCodesControllerTest extends SlimAppTest
{
    public $applications ;

    public $authorizationCodes ;

    public $usersAuthApps ;

    public function setUp()
    {
        parent::setUp();

        $this->applications       = $this->createMock( Applications::class ) ;
        $this->authorizationCodes = $this->createMock( AuthorizationCodes::class ) ;
        $this->usersAuthApps      = $this->createMock( UsersAuthApps::class ) ;
    }

    public function testAuthorizeEmpty()
    {
        // GET
        $request = $this->getRequest( 'GET' , '/oauth/authorize' ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/authorize' ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testAuthorizeNotComplete()
    {
        $data_request =
        [
            'response_type' => 'code'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/authorize' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/authorize' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testAuthorizeBadResponseType()
    {
        $data_request =
        [
            'client_id'     => 'ci' ,
            'redirect_uri'  => 'ru' ,
            'response_type' => 'auth'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/authorize' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/authorize' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testAuthorizeInvalidClientId()
    {
        // fake db result
        $this->applications->method( 'getByProperty' )
            ->will( $this->returnValue( null ) ) ;

        $this->container['applications'] = $this->applications ;

        $data_request =
        [
            'client_id'     => 'ci' ,
            'redirect_uri'  => 'ru' ,
            'response_type' => 'code'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/authorize' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 401 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/authorize' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 401 , $response->getStatusCode() ) ;
    }

    public function testAuthorizeRedirectNotMatch()
    {
        // fake db result
        $rowDB = (object)
        [
            'client_id'    => '1234567890' ,
            'url_redirect' => ''
        ] ;

        $this->applications->method( 'getByProperty' )
            ->will( $this->returnValue( $rowDB ) ) ;

        $this->container['applications'] = $this->applications ;

        $data_request =
        [
            'client_id'     => '1234567890' ,
            'redirect_uri'  => 'ru' ,
            'response_type' => 'code'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/authorize' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/authorize' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testAuthorizeRedirectCode()
    {
        // fake db results
        $rowDB = (object)
        [
            'client_id'    => '1234567890' ,
            'url_redirect' => 'http://localhost'
        ] ;

        $this->applications->method( 'getByProperty' )
            ->will( $this->returnValue( $rowDB ) ) ;

        $this->applications->method( 'isWhitelisted' )
            ->will( $this->returnValue( true ) ) ;

        $this->authorizationCodes->method( 'insert' )
            ->will( $this->returnValue( true ) ) ;

        $this->usersAuthApps->method( 'getUserApp' )
            ->will( $this->returnValue( null ) ) ;

        $this->container['applications']       = $this->applications ;
        $this->container['authorizationCodes'] = $this->authorizationCodes ;
        $this->container['usersAuthApps']      = $this->usersAuthApps ;

        $data_request =
        [
            'client_id'     => '1234567890' ,
            'redirect_uri'  => 'http://localhost' ,
            'response_type' => 'code'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/authorize' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 302 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/authorize' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 302 , $response->getStatusCode() ) ;
    }

    public function testAuthorizeAskUserPermission()
    {
        // fake db results
        $rowDB = (object)
        [
            'client_id'    => '1234567890' ,
            'url_redirect' => 'http://localhost'
        ] ;

        $this->applications->method( 'getByProperty' )
            ->will( $this->returnValue( $rowDB ) ) ;

        $this->applications->method( 'isWhitelisted' )
            ->will( $this->returnValue( false ) ) ;

        $this->authorizationCodes->method( 'insert' )
            ->will( $this->returnValue( true ) ) ;

        $this->usersAuthApps->method( 'getUserApp' )
            ->will( $this->returnValue( null ) ) ;

        $this->container['applications']       = $this->applications ;
        $this->container['authorizationCodes'] = $this->authorizationCodes ;
        $this->container['usersAuthApps']      = $this->usersAuthApps ;

        $data_request =
        [
            'client_id'     => '1234567890' ,
            'redirect_uri'  => 'http://localhost' ,
            'response_type' => 'code'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/authorize' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/authorize' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
    }

    public function testAuthorizeAlreadyAskUserPermission()
    {
        // fake db results
        $rowDB = (object)
        [
            'client_id'    => '1234567890' ,
            'url_redirect' => 'http://localhost'
        ] ;

        $rowUsersAuthApps = (object)
        [
            'scope' => 'rights'
        ];

        $this->applications->method( 'getByProperty' )
            ->will( $this->returnValue( $rowDB ) ) ;

        $this->applications->method( 'isWhitelisted' )
            ->will( $this->returnValue( false ) ) ;

        $this->authorizationCodes->method( 'insert' )
            ->will( $this->returnValue( true ) ) ;

        $this->usersAuthApps->method( 'getUserApp' )
            ->will( $this->returnValue( $rowUsersAuthApps ) ) ;

        $this->container['applications']       = $this->applications ;
        $this->container['authorizationCodes'] = $this->authorizationCodes ;
        $this->container['usersAuthApps']      = $this->usersAuthApps ;

        $data_request =
        [
            'client_id'     => '1234567890' ,
            'redirect_uri'  => 'http://localhost' ,
            'response_type' => 'code' ,
            'scope'         => 'rights'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/authorize' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 302 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/authorize' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->authorize( $request , $response ) ;

        $this->assertEquals( 302 , $response->getStatusCode() ) ;
    }

    public function testAllowApplication()
    {
        // fake db results
        $this->authorizationCodes->method( 'insert' )
            ->will( $this->returnValue( true ) ) ;

        $this->usersAuthApps->method( 'insert' )
            ->will( $this->returnValue( true ) ) ;

        $this->container['authorizationCodes'] = $this->authorizationCodes ;
        $this->container['usersAuthApps']      = $this->usersAuthApps ;

        $data_request =
        [
            'client_id'     => '1234567890' ,
            'redirect_uri'  => 'http://localhost' ,
            'response_type' => 'code' ,
            'scope'         => 'rights'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/allowApplication' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->allowApplication( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/allowApplication' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->authorizationCodeController->allowApplication( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
    }
}