<?php

namespace com\ooopener\controllers\oauth;


use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\Controller ;

use Firebase\JWT\JWT;
use Slim\Container;

use Exception ;

/**
 * The revoke controller.
 */
class RevokeController extends Controller
{
    /**
     * Creates a new class RevokeController extends Controller
    instance.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct( $container );
    }

    const ARGUMENTS_REVOKE_DEFAULT =
    [
        'token' => NULL
    ];

    /**
     * Revoke token from GET and POST request
     *
     * @param Request $request The request
     * @param Response $response The response
     * @param array $args The arguments
     *
     * @return mixed
     */
    public function revoke( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $this->logger->debug( $this . ' revoke' ) ;

        extract( array_merge( self::ARGUMENTS_REVOKE_DEFAULT , $args ) ) ;

        // check get params
        if( isset( $request ) )
        {
            $params = $request->getQueryParams() ;
            if( !$params )
            {
                $params = $request->getParsedBody() ;
            }

            $token = isset( $params['token'] ) ? $params['token'] : NULL ;
        }
        else
        {
            $token = $args['token'] ;
        }

        //// check required parameters

        if( $token == NULL )
        {
            return $this->error( $response ,"The request is missing a required parameter " , "400" );
        }

        try
        {
            //// check if token exists and not expired

            $algorithm   = $this->container->settings['token']['algorithm'] ;
            $secret      = $this->container->settings['token']['key'] ;

            $jwt = JWT::decode( $token , $secret , $algorithm ) ;

            if( $jwt && $jwt->jti )
            {
                $this->logger->debug( $this . " token id => " . $jwt->jti ) ;
                $agent = $this->container->userInfos->getUserAgent() ;
                $check = $this->container->sessions->check( $jwt->jti , $agent ) ;

                if( $check )
                {
                    // revoke token
                    $this->container->sessions->revoke( $jwt->jti ) ;

                    return $this->success( $response ,"ok" ) ;
                }
            }

            return $this->formatError( $response , "500" , NULL , NULL , 500 ) ;

        }
        catch( Exception $e )
        {
            return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ] , NULL , 500 ) ;
        }
    }

}


