<?php

use Firebase\JWT\JWT;

use Slim\Http\Response ;

class LoginLogoutControllerTest extends SlimAppTest
{

    public function testLogin()
    {
        $request = $this->getRequest( 'GET' , '/login' ) ;

        $response = new Response() ;
        $response = $this->container->loginController->get( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
    }

    public function testRegister()
    {
        $data_request =
        [
            'token'    => JWT::encode( null , $this->container->settings['token']['key'] , $this->container->settings['token']['algorithm'][0] ) ,
            'provider' => 'blabla'
        ] ;

        $request = $this->getRequest( 'POST' , '/register' , null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->loginController->post( $request , $response ) ;

        $this->assertEquals( 401 , $response->getStatusCode() ) ;
    }

    public function testLogout()
    {
        $request = $this->getRequest( 'GET' , '/logout' ) ;

        $response = new Response() ;
        $response = $this->container->logoutController->get( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
    }


}