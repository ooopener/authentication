<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/oauth/authorize' ,
    [ $container->authorizationCodeController , 'authorize' ]
)
->setName( 'oauth.authorize' ) ;

$application->post
(
    '/oauth/authorize' , [ $container->authorizationCodeController , 'authorize' ]
)
->setName( 'oauth.authorize.post' ) ;
