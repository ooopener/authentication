<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/oauth/token' ,
    [ $container->tokenController , 'token' ]
)
->setName( 'oauth.token' ) ;

$application->post
(
    '/oauth/token' , [ $container->tokenController , 'token' ]
)
->setName( 'oauth.token.post' ) ;
