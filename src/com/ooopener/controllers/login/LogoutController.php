<?php

namespace com\ooopener\controllers\login ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\Controller ;
use Slim\Container ;

use DateTime ;
use Exception ;

/**
 * The logout controller
 * @package com\ooopener\controllers\oauth
 */
class LogoutController extends Controller
{

    /**
     * LogoutController constructor.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct( $container );
    }

    public function get( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $redirect_uri = NULL ;

        $client_id = NULL ;

        if( isset( $request ) )
        {
            $params = $request->getQueryParams() ;

            $redirect_uri  = isset( $params['redirect_uri'] ) ? $params['redirect_uri'] : NULL;

            if( isset( $params['client_id'] ) )
            {
                $client_id = $params['client_id'] ;
            }
        }
        else
        {
            $redirect_uri  = $args['redirect_uri'];
        }

        if( isset( $_SESSION[ $this->container->settings['auth']['session'] ] ) )
        {
            $user_id = $_SESSION[ $this->container->settings['auth']['session'] ] ;

            // get user ip
            $ip         = $this->container->userInfos->getIp() ;
            $user_agent = $this->container->userInfos->getUserAgent() ;

            if( isset( $client_id ) )
            {
                // logout access token
                try
                {
                    $this->container->sessions->logoutAccessToken( $user_id , $client_id , $ip , $user_agent ) ;
                }
                catch( Exception $e )
                {
                    $this->logger->warn( $this . " " . $e->getMessage() ) ;
                }
            }

            // logout id token
            try
            {
                $this->container->sessions->logoutIDToken( $user_id , $ip , $user_agent ) ;
            }
            catch( Exception $e )
            {
                $this->logger->warn( $this . " " . $e->getMessage() ) ;
            }

            // remove session
            unset( $_SESSION[ $this->container->settings['auth']['session'] ] );
        }

        // remove cookie
        $cookie = $this->container->cookie->get( $request , $this->container->settings['token']['cookie_name'] ) ;

        if( $cookie )
        {
            # expire cookie and web browser will remove it
            $response = $this->container->cookie->set
            (
                $response ,
                $this->container->settings['token']['cookie_name'] ,
                null ,
                new DateTime( '-5 years' ) ,
                '/' . $this->container->settings['version']
            );
        }

        $firebase = $this->container->firebaseConfig;
        $settings = $this->container->settings;

        $response = $this->container->cache->denyCache( $response ) ;

        return $this->render
        (
            $response ,
            'login/logout.twig' ,
            [
                'apiKey'            => $firebase['apiKey'],
                'authDomain'        => $firebase['authDomain'],
                'databaseURL'       => $firebase['databaseURL'],
                'storageBucket'     => $firebase['storageBucket'],
                'messagingSenderId' => $firebase['messagingSenderId'],
                'loginUri'          => $settings['app']['path'] . $this->container->router->pathFor( 'api.login' ) ,
                'redirectUri'       => $redirect_uri
            ]
        ) ;
    }
}
