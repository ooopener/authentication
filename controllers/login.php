<?php

use com\ooopener\controllers\login\InviteController;
use com\ooopener\controllers\login\LoginController;
use com\ooopener\controllers\login\LogoutController;

$container['inviteController'] = function( $container )
{
    return new InviteController( $container ) ;
};

$container['loginController'] = function( $container )
{
    return new LoginController( $container ) ;
};

$container['logoutController'] = function( $container )
{
    return new LogoutController( $container ) ;
};