<?php

namespace com\ooopener\middlewares ;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\Controller;
use Slim\Container;

class Authorized extends Controller
{
    public function __construct( Container $container )
    {
        parent::__construct( $container );
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $next
     *
     * @return Response
     */
    public function __invoke( Request $request , Response $response , $next )
    {
        //$this->logger->debug( $this . ' isAuthorized' ) ;

        $authorized = FALSE ;

        $path = $request->getUri()->getPath() ;
        $method = $request->getMethod() ;

        $passthrough = $this->config['token']['passthrough'] ;

        // passthrough 'OPTIONS' method
        if( $method == 'OPTIONS' )
        {
            $authorized = TRUE ;
        }

        // handle index
        if( $path == "/" )
        {
            $authorized = TRUE ;
        }

        // passthrough for paths
        if( $this->checkPassthroughPath( "/" . $path , $passthrough ) )
        {
            $authorized = TRUE ;
        }

        // check permissions
        if( isset( $this->container->jwt ) && isset( $this->container->jwt->scope ) )
        {
            $scope = json_decode( $this->container->jwt->scope ) ;

            // get main path and if exists get the resource's identifier
            preg_match( '/(?<name>\w+)(\/)?(?<digit>\d+)?/' , $path , $matches ) ;

            $resource = array_key_exists( 'digit' , $matches ) ? '/' . $matches['digit'] : '' ;

            $name = isset( $matches['name'] ) ? $matches['name'] : '' ;

            $path = $name . $resource ;

            if( $scope != null  && ( property_exists( $scope , $name ) || property_exists( $scope , $path ) ) )
            {
                if( property_exists( $scope , $path ) )
                {
                    $authorize = $scope->{$path} ;
                }
                else
                {
                    $authorize = $scope->{$matches['name']} ;
                }

                switch( $authorize )
                {
                    case 'admin':
                        $authorized = TRUE ;
                        break;
                    case 'write':
                        if( $method == 'GET' || $method == 'HEAD' || $method == 'PATCH' || $method == 'POST' || $method == 'PUT' || $method == 'DELETE' )
                        {
                            $authorized = TRUE ;
                        }
                        break;
                    case 'read':
                        if( $method == 'GET' || $method == 'HEAD' )
                        {
                            $authorized = TRUE ;
                        }
                        break;
                }
            }
        }

        if( $authorized == FALSE )
        {
            $format = $this->container['format'] ;

            switch( $format )
            {
                case 'json' :
                    return $this->formatError( $response , "401" , NULL , NULL , 401 ) ;
                    break;
                case 'html' :
                default     :
                    return $this->pathFor( $response , 'api.index' );
                    break ;
            }
        }
        else
        {
            $response = $next( $request , $response ) ;
            return $response;
        }

    }

    private function checkPassthroughPath( string $uri , array $list )
    {
        foreach( $list as $passthrough )
        {
            $passthrough = rtrim($passthrough, "/") ;
            if( preg_match("@^{$passthrough}(/.*)?$@" , $uri ) )
            {
                return true ;
            }
        }
        return false ;
    }
}