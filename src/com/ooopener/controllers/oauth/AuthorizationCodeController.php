<?php

namespace com\ooopener\controllers\oauth;

use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use com\ooopener\controllers\Controller ;

use Slim\Container;

use Exception;

/**
 * The authorization code controller.
 */
class AuthorizationCodeController extends Controller
{
    /**
     * Creates a new AuthorizationCodeController instance.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct( $container );
    }

    const ARGUMENTS_AUTHORIZE_DEFAULT =
    [
        'client_id'     => NULL ,
        'redirect_uri'  => NULL ,
        'response_type' => 'code' ,
        'scope'         => NULL ,
        'state'         => NULL
    ];

    /**
     * Handle authorize from GET and POST request
     *
     * @param Request $request The request
     * @param Response $response The response
     * @param array $args The arguments
     *
     * @return mixed
     */
    public function authorize( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $this->logger->debug($this . ' authorize');

        if( !isset( $_SESSION[ $this->container->settings['auth']['session'] ] ) )
        {
            // save to redirect
            $fullUrl = $request->getUri()->getBaseUrl()
                . '/'
                . $request->getUri()->getPath()
                . '?' . $request->getUri()->getQuery() ;

            $_SESSION[ "urlRedirect" ] = $fullUrl ;

            $params = $request->getParsedBody();
            if( $params )
            {
                $_SESSION[ "client_id" ]     = isset( $params['client_id'] )     ? $params['client_id']     : NULL ;
                $_SESSION[ "redirect_uri" ]  = isset( $params['redirect_uri'] )  ? $params['redirect_uri']  : NULL ;
                $_SESSION[ "response_type" ] = isset( $params['response_type'] ) ? $params['response_type'] : NULL ;
                $_SESSION[ "scope" ]         = isset( $params['scope'] )         ? $params['scope']         : NULL ;
                $_SESSION[ "state" ]         = isset( $params['state'] )         ? $params['state']         : NULL ;
            }

            // go to login
            $response = $this->container->cache->denyCache( $response ) ;

            return $response->withRedirect( $this->container->router->pathFor( "api.login" ) ) ;
        }

        extract( array_merge( self::ARGUMENTS_AUTHORIZE_DEFAULT , $args ) ) ;

        // check get params
        if( isset( $request ) )
        {
            // params from GET
            $params = $request->getQueryParams();
            if( !$params )
            {
                // params from POST
                $params = $request->getParsedBody();
            }

            if( $params )
            {
                $client_id     = isset( $params['client_id'] )     ? $params['client_id']     : NULL ;
                $redirect_uri  = isset( $params['redirect_uri'] )  ? $params['redirect_uri']  : NULL ;
                $response_type = isset( $params['response_type'] ) ? $params['response_type'] : NULL ;
                $scope         = isset( $params['scope'] )         ? $params['scope']         : NULL ;
                $state         = isset( $params['state'] )         ? $params['state']         : NULL ;
            }
            else
            {
                $client_id     = isset( $_SESSION['client_id'] )     ? $_SESSION['client_id']     : NULL ;
                $redirect_uri  = isset( $_SESSION['redirect_uri'] )  ? $_SESSION['redirect_uri']  : NULL ;
                $response_type = isset( $_SESSION['response_type'] ) ? $_SESSION['response_type'] : NULL ;
                $scope         = isset( $_SESSION['scope'] )         ? $_SESSION['scope']         : NULL ;
                $state         = isset( $_SESSION['state'] )         ? $_SESSION['state']         : NULL ;
            }
        }

        //// check required parameters
        try
        {
            if( $client_id     == NULL ) throw new Exception('client_id' ) ;
            if( $redirect_uri  == NULL ) throw new Exception('redirect_uri' ) ;
            if( $response_type == NULL ) throw new Exception('response_type' ) ;
        }
        catch( Exception $error )
        {
            $this->cleanSessionValues() ;
            return $this->error( $response ,"The request is missing a required parameter : " . $error->getMessage() , "400" );
        }

        //// check response type value
        if( $response_type != "code" )
        {
            $this->cleanSessionValues() ;
            return $this->error( $response ,"Response type must be code" , "400" );
        }

        try
        {
            //// check application exists

            $application = $this->container->applications->getByProperty( 'oAuth.client_id' , $client_id  ) ;

            if( !$application )
            {
                $this->cleanSessionValues() ;
                return $this->error( $response ,"Invalid client id " , "401" , null , 401 );
            }

            // check redirect URL if match one of URLs of the application

            $urls = explode( ',' , $application->oAuth['url_redirect'] ) ;

            if( !in_array( $redirect_uri , $urls ) )
            {
                $this->cleanSessionValues() ;
                return $this->error( $response , "The redirect URI did not match a registered redirect URI" ) ;
            }

            // check if user already authorize the application

            $isWhitelisted = $this->container->applications->isWhitelisted( $client_id ) ;

            $userId = $_SESSION[ $this->container->settings['auth']['session'] ] ;

            $authApp = $this->container->usersAuthApps->getUserApp( $userId , $client_id );

            if( $isWhitelisted || ( $authApp && $authApp->scope == $scope ) )
            {
                $this->cleanSessionValues() ;

                // return authorization code to redirect uri
                $code = $this->authorizationCode( $userId , $client_id ) ;

                $options = [ "code" => $code ];
                if( isset( $state ) )
                {
                    $options["state"] = $state;
                }
                $url = $this->buildUri( $redirect_uri , [ 'query' => $options ] ) ;

                $response = $this->container->cache->denyCache( $response ) ;
                return $response->withRedirect( $url ) ;
            }
            else
            {
                $_SESSION['client_id']     = $client_id;
                $_SESSION['redirect_uri']  = $redirect_uri;
                $_SESSION['response_type'] = $response_type;
                $_SESSION['scope']         = $scope;
                $_SESSION['state']         = $state;

                $user = $this->container->users->get( $userId , [ 'key' => 'uuid' ] );

                $response = $this->container->cache->denyCache( $response ) ;

                return $this->render
                (
                    $response,
                    'oauth/authorize.twig',
                    [
                        "appName"   => $application->name,
                        "userEmail" => $user->email
                    ]
                );
            }
        }
        catch( Exception $e )
        {
            $this->cleanSessionValues() ;
            return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ]  , NULL , 500 );
        }
    }

    /**
     * Allow application
     *
     * @param Request $request The request
     * @param Response $response The response
     * @param array $args The arguments
     *
     * @return mixed
     */
    public function allowApplication( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $this->logger->debug($this . ' allowApplication');

        // get parameters in session
        $client_id     = $_SESSION['client_id'] ;
        $redirect_uri  = $_SESSION['redirect_uri'] ;
        $response_type = $_SESSION['response_type'] ;
        $scope         = $_SESSION['scope'] ;
        $state         = $_SESSION['state'] ;

        $userId = $_SESSION[ $this->container->settings['auth']['session'] ] ;

        try
        {
            $userAuthApp =
            [
                "user_id"   => $userId ,
                "client_id" => $client_id ,
                "scope"     => $scope
            ];

            $saved = $this->container->usersAuthApps->insert( $userAuthApp );

            $this->logger->debug($this . ' inserted => ' . $saved );

            // return authorization code to redirect uri
            $code = $this->authorizationCode( $userId , $client_id ) ;

            $options = [ "code" => $code ];
            if( isset( $state ) )
            {
                $options["state"] = $state;
            }
            $url = $this->buildUri( $redirect_uri , [ 'query' => $options ] ) ;

            $this->cleanSessionValues();

            return $this->success( $response , "ok" , '' , NULL ,  [ "redirect" => $url ] ) ;
        }
        catch( Exception $e )
        {
            $this->cleanSessionValues();
            return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ]  , NULL , 500 );
        }
    }

    /**
     * Generate authorization code and save it in db
     *
     * @param string $user_id
     * @param string $client_id
     *
     * @throws
     * @return string
     */
    private function authorizationCode( $user_id , $client_id )
    {
        // generate authorization code
        $code = bin2hex( random_bytes( 16 ) ) ;

        $auth_code =
        [
            "user_id"   => $user_id ,
            "client_id" => $client_id ,
            "code"      => $code ,
            "ip"        => $this->container->userInfos->getIp()
        ];

        $this->container->authorizationCodes->insert( $auth_code );

        return $code ;
    }

    private function cleanSessionValues()
    {
        unset( $_SESSION['client_id'] ) ;
        unset( $_SESSION['redirect_uri'] ) ;
        unset( $_SESSION['response_type'] ) ;
        unset( $_SESSION['scope'] ) ;
        unset( $_SESSION['state'] ) ;
    }

}


