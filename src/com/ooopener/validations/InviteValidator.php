<?php

namespace com\ooopener\validations;

use Slim\Container;

use Exception ;

class InviteValidator extends Validation
{
    public function __construct( Container $container )
    {
        parent::__construct( $container ) ;

        $this->addFieldMessages
        ([
            'team' =>
            [
                'team' => 'Select a valid team.'
            ],
            'person' =>
            [
                'person' => 'Select a valid person'
            ],
            'email' =>
            [
                'email' => 'email must be a valid email address.',
                'userEmail' => 'email already used'
            ],
            'redirect' =>
            [
                'redirect' => 'redirect is not valid'
            ]
        ]);
    }

    /**
     * Validates if the email is registered.
     *
     * @param string $value
     * @param string $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_userEmail( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return !$this->container->users->exist( $value , [ 'key' => 'email' ] ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_emailRegistered failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the person is registered.
     *
     * @param string $value
     * @param string $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_person( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->people->exist( $value , 'id' ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_person failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the redirect is registered.
     *
     * @param string $value
     * @param string $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_redirect( $value , $input , $args )
    {
        try
        {

            $jwt = $this->container->jwt ;

            if( property_exists( $jwt , 'aud' ) )
            {
                $application = $this->container->applications->getByProperty( 'client_id' , $jwt->aud ) ;

                if( $application )
                {
                    $urls = explode( ',' , $application->url_redirect ) ;

                    $pattern = "@^" . preg_quote( $value ) . "@" ;
                    $search = preg_grep( $pattern , $urls ) ;

                    if( !empty($search) )
                    {
                        return TRUE ;
                    }
                }
            }
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_redirect failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }

    /**
     * Validates if the team is registered.
     *
     * @param string $value
     * @param string $input
     * @param array $args
     *
     * @return bool
     */
    public function validate_team( $value , $input , $args )
    {
        $required = FALSE ;

        if( $args && count($args) > 0 )
        {
            $required = ($args[0] == 'required') ;
        }

        if( !$required )
        {
            if( $value == NULL || empty($value) )
            {
                return TRUE ;
            }
        }

        try
        {
            return $this->container->teams->exist( $value ) ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warn
            (
                $this . ' validate_team failed, value:' . json_encode($value)
                . ' args:' . json_encode($args)
                . ' error:' . $e->getMessage()
            ) ;
        }

        return FALSE ;
    }
}


