<?php

$application->post
(
    '/oauth/allowApplication' , [ $container->authorizationCodeController , 'allowApplication' ]
)
->add( 'csrf' )
->setName( 'oauth.allowApplication.post' ) ;
