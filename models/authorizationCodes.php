<?php

use com\ooopener\models\AuthorizationCodes ;

$container['authorizationCodes'] = function( $container )
{
    return new AuthorizationCodes( $container , "authorization_codes" ) ;
};

