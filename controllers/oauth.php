<?php

use com\ooopener\controllers\oauth\AuthorizationCodeController;
use com\ooopener\controllers\oauth\RevokeController;
use com\ooopener\controllers\oauth\TokenController;

$container['authorizationCodeController'] = function( $container )
{
    return new AuthorizationCodeController( $container ) ;
};

$container['revokeController'] = function( $container )
{
    return new RevokeController( $container ) ;
};

$container['tokenController'] = function( $container )
{
    return new TokenController( $container ) ;
};

