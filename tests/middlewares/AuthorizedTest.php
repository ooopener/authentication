<?php

use Slim\Http\Response ;

use com\ooopener\middlewares\Authorized ;

class AuthorizedTest extends SlimAppTest
{

    public $authorized ;

    public function setUp()
    {
        parent::setUp() ;
        $this->authorized = new Authorized( $this->container ) ;
    }

    public function testAuthorized()
    {
        $next = function( $req , $res )
        {
            return $res ;
        };

        $request = $this->getRequest( 'GET' , '/places' , [ 'format' => 'json' ] ) ;

        $response = new Response() ;
        $response = $this->authorized->__invoke( $request , $response , $next ) ;

        $this->assertEquals( 401 , $response->getStatusCode() ) ;
    }

    public function testAuthorizedPassthrough()
    {
        $passthrough = $this->container->settings['token']['passthrough'] ;
        $passthroughAuthorized = $this->container->settings['token']['passthroughAuthorized'] ;

        $passthrough = array_merge( $passthrough , $passthroughAuthorized ) ;

        $next = function( $req , $res )
        {
            return $res ;
        };

        foreach( $passthrough as $key => $value )
        {
            //$value = substr( $value , 1 ) ;
            $request = $this->getRequest( 'GET' , $value , [ 'format' => 'json' ] ) ;

            $response = new Response() ;
            $response = $this->authorized->__invoke( $request , $response , $next ) ;

            $this->assertEquals( 200 , $response->getStatusCode() ) ;
        }

        // check with different methods

        $methods = [ 'GET' => 401 , 'HEAD' => 401 , 'POST' => 401 , 'PUT' => 401 , 'DELETE' => 401 , 'OPTIONS' => 200 ] ;

        foreach( $methods as $key => $value )
        {
            $request = $this->getRequest( $key , '/places' , [ 'format' => 'json' ] ) ;

            $response = new Response() ;
            $response = $this->authorized->__invoke( $request , $response , $next ) ;

            $this->assertEquals( $value , $response->getStatusCode() ) ;
        }
    }

    public function testAuthorizedScope()
    {
        $next = function( $req , $res )
        {
            return $res ;
        };

        $scopes =
        [
            'admin' =>
            [
                'scope'   => [ 'places' => 'admin' ] ,
                'methods' =>
                [
                    'GET'    => 200 ,
                    'HEAD'   => 200 ,
                    'POST'   => 200 ,
                    'PUT'    => 200 ,
                    'DELETE' => 200
                ]
            ],
            'admin_resource' =>
            [
                'scope'   => [ 'places/54' => 'admin' ] ,
                'methods' =>
                [
                    'GET'    => 200 ,
                    'HEAD'   => 200 ,
                    'POST'   => 200 ,
                    'PUT'    => 200 ,
                    'DELETE' => 200
                ]
            ],
            'write' =>
            [
                'scope'   => [ 'places' => 'write' ] ,
                'methods' =>
                [
                    'GET'    => 200 ,
                    'HEAD'   => 200 ,
                    'POST'   => 200 ,
                    'PUT'    => 200 ,
                    'DELETE' => 200
                ]
            ],
            'write_resource' =>
            [
                'scope'   => [ 'places/63' => 'write' ] ,
                'methods' =>
                [
                    'GET'    => 200 ,
                    'HEAD'   => 200 ,
                    'POST'   => 200 ,
                    'PUT'    => 200 ,
                    'DELETE' => 200
                ]
            ],
            'read' =>
            [
                'scope'   => [ 'places' => 'read' ] ,
                'methods' =>
                [
                    'GET'    => 200 ,
                    'HEAD'   => 200 ,
                    'POST'   => 401 ,
                    'PUT'    => 401 ,
                    'DELETE' => 401
                ]
            ],
            'read_resource' =>
            [
                'scope'   => [ 'places/751' => 'read' ] ,
                'methods' =>
                [
                    'GET'    => 200 ,
                    'HEAD'   => 200 ,
                    'POST'   => 401 ,
                    'PUT'    => 401 ,
                    'DELETE' => 401
                ]
            ],
            'deny' =>
            [
                'scope'   => [ 'places' => 'deny' ] ,
                'methods' =>
                [
                    'GET'    => 401 ,
                    'HEAD'   => 401 ,
                    'POST'   => 401 ,
                    'PUT'    => 401 ,
                    'DELETE' => 401
                ]
            ],
            'deny_resource' =>
            [
                'scope'   => [ 'places/123' => 'deny' ] ,
                'methods' =>
                [
                    'GET'    => 401 ,
                    'HEAD'   => 401 ,
                    'POST'   => 401 ,
                    'PUT'    => 401 ,
                    'DELETE' => 401
                ]
            ]
        ] ;

        foreach( $scopes as $key => $value )
        {
            $this->setScope( $value['scope'] ) ;

            $path = key( $value['scope'] ) ;

            foreach( $value['methods'] as $keyBis => $valueBis )
            {
                $request = $this->getRequest( $keyBis , $path , [ 'format' => 'json' ] ) ;

                $response = new Response() ;
                $response = $this->authorized->__invoke( $request , $response , $next ) ;

                $this->assertEquals( $valueBis , $response->getStatusCode() ) ;
            }
        }
    }

    private function setScope( array $permissions )
    {
        $scope = json_encode( $permissions ) ;

        $jwt = (object) [ "scope" => $scope ] ;
        $this->container->jwt = $jwt ;
    }
}
