<?php

namespace com\ooopener\helpers\token;

use ReflectionClass ;

class GrantType
{
    /**
     * GrantType constructor.
     */
    public function __construct()
    {
    }

    const AUTHORIZATION_CODE = 'authorization_code' ;
    const CLIENT_CREDENTIALS = 'client_credentials' ;
    const REFRESH_TOKEN      = 'refresh_token' ;

    static function getConstants()
    {
        $oClass = new ReflectionClass( __CLASS__ ) ;
        return $oClass->getConstants() ;
    }
}