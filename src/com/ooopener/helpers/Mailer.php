<?php

namespace com\ooopener\helpers ;

use Exception ;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as MailerException;

use Slim\Container;

class Mailer
{
    public function __construct( Container $container , $config )
    {
        $this->container = $container ;

        $this->mailer = new PHPMailer( true ) ;

        $this->mailer->IsSMTP();

        $this->mailer->Host       = $config['host'] ;
        $this->mailer->SMTPAuth   = $config['smtp_auth'] ;
        $this->mailer->Username   = $config['username'] ;
        $this->mailer->Password   = $config['password'] ;
        $this->mailer->SMTPSecure = $config['smtp_secure'] ;
        $this->mailer->Port       = $config['port'] ;

        $this->mailer->SetFrom
        (
            $config['from'],
            $config['from_name'],
            FALSE
        );

        $this->mailer->isHTML( $config['html'] ) ;
        $this->mailer->CharSet = $config['charset'] ;
    }

    public $container ;

    public $mailer ;

    public function sendInvitation( $invite )
    {
        try
        {
            $this->mailer->Subject = 'Invitation' ;

            $this->mailer->addAddress( $invite['email'] ) ;

            $this->mailer->Body = $this->container->view->fetch
            (
                'emails/invite.twig' ,
                [
                    'app_name' => $this->container->settings['name'] ,
                    'app_url'  => $this->container->settings['app']['url'] ,
                    'code'     => $invite['code'] ,
                    'name'     => $invite['name'] ,
                    'redirect' => $invite['redirect']
                ]
            ) ;

            $this->mailer->send() ;

            return true ;
        }
        catch( MailerException $e )
        {
            $this->container->logger->warning( "Error with sending email : " . $this->mailer->ErrorInfo ) ;
            return false ;
        }
        catch( Exception $e )
        {
            $this->container->logger->warning( "Error email template : " . $e->getMessage() ) ;
            return false ;
        }
    }
}