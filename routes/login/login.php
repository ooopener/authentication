<?php

$application->map
(
    [ 'GET' , 'OPTIONS' ] ,
    '/login' , [ $container->loginController , 'get'   ]
)
->setName('api.login') ;

$application->post
(
    '/login/register' , [ $container->loginController , 'post'   ]
)
->add( 'csrf' )
->setName('api.login.register.post') ;

