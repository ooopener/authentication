<?php

use Firebase\JWT\JWT as JwtToken;

use Slim\Http\Response ;

use com\ooopener\middlewares\Jwt ;

use com\ooopener\things\TokenPayload;

class JwtTest extends SlimAppTest
{
    public $jwt ;

    public function setUp()
    {
        parent::setUp() ;
        $this->jwt = new Jwt( $this->container ) ;
    }

    public function testWithoutToken()
    {
       $this->tokenRequest() ;
    }

    public function testWithIncorrectToken()
    {
        $this->tokenRequest( 'fffffretrtyhtyzerp' ) ;
    }

    public function testWithInvalidToken()
    {
        $this->tokenRequest( 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ' ) ;
    }

    public function testWithValidToken()
    {
        $payload = new TokenPayload() ;

        $token = JwtToken::encode( $payload , $this->container->settings['token']['key'] , $this->container->settings['token']['algorithm'][0] ) ;
        $this->tokenRequest( $token , 200 ) ;
    }

    private function tokenRequest( string $token = null , int $code = 401 )
    {
        $next = function( $req , $res )
        {
            return $res ;
        };

        $methods = [ 'GET' , 'HEAD' , 'POST' , 'PUT' , 'DELETE' ] ;
        $types   = [ 'header' , 'cookie' ] ;

        foreach( $types as $type )
        {
            foreach( $methods as $method )
            {
                if( $type == 'header' )
                {
                    $request = $this->getRequest( $method , '/places' , [ 'format' => 'json' ] , null , null , $token ) ;
                }
                else
                {
                    $request = $this->getRequest( $method , '/places' , [ 'format' => 'json' ] , null , [ $this->container->settings['token']['cookie_name'] => $token ] ) ;
                }

                $response = new Response() ;
                $response = $this->jwt->__invoke( $request , $response , $next ) ;

                $this->assertEquals( $code , $response->getStatusCode() ) ;
            }
        }
    }
}