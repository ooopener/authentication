<?php

use Firebase\JWT\JWT ;

use Slim\Http\Response ;

use com\ooopener\models\Tokens ;

use com\ooopener\things\TokenPayload;

class RevokeControllerTest extends SlimAppTest
{
    public $tokens ;

    public function setUp()
    {
        parent::setUp();

        $this->tokens = $this->createMock( Tokens::class ) ;
    }

    public function testRevokeEmpty()
    {
        // GET
        $request = $this->getRequest( 'GET' , '/oauth/revoke' ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/revoke' ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testRevokeInvalidToken()
    {
        $data_request =
        [
            'token' => '1234567890'
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/revoke' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/revoke' ,null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testRevokeInvalidTokenID()
    {
        $payload = new TokenPayload() ;
        $token = Jwt::encode( $payload , $this->container->settings['token']['key'] , $this->container->settings['token']['algorithm'][0] ) ;

        $data_request =
        [
            'token' => $token
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/revoke' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/revoke' ,null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testRevokeNotExistsToken()
    {
        // fake db result
        $this->tokens->method( 'check' )
            ->will( $this->returnValue( false ) ) ;

        $this->container->tokens = $this->tokens ;

        $payload = new TokenPayload() ;
        $payload->jti = 'testJTI' ;

        $token = Jwt::encode( $payload , $this->container->settings['token']['key'] , $this->container->settings['token']['algorithm'][0] ) ;

        $data_request =
        [
            'token' => $token
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/revoke' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/revoke' ,null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 400 , $response->getStatusCode() ) ;
    }

    public function testRevokeTokenValid()
    {
        // fake db result
        $this->tokens->method( 'check' )
            ->will( $this->returnValue( true ) ) ;

        $this->tokens->method( 'revoke' )
            ->will( $this->returnValue( true ) ) ;

        $this->container->tokens = $this->tokens ;

        $payload = new TokenPayload() ;
        $payload->jti = 'testJTI' ;

        $token = Jwt::encode( $payload , $this->container->settings['token']['key'] , $this->container->settings['token']['algorithm'][0] ) ;

        $data_request =
        [
            'token' => $token
        ];

        // GET
        $request = $this->getRequest( 'GET' , '/oauth/revoke' , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;

        // POST
        $request = $this->getRequest( 'POST' , '/oauth/revoke' ,null , $data_request ) ;

        $response = new Response() ;
        $response = $this->container->revokeController->revoke( $request , $response ) ;

        $this->assertEquals( 200 , $response->getStatusCode() ) ;
    }
}