<?php

namespace com\ooopener\helpers;

use Psr\Http\Message\RequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;


use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\FigRequestCookies;
use Dflydev\FigCookies\SetCookie;

use DateTime;

/**
 * Invoked to manage the cookies of the application. Check https://github.com/dflydev/dflydev-fig-cookies
 */
class CookieHelper
{
    /**
     * Creates a new CookieManager instance.
     */
    public function __construct()
    {

    }

    /**
     * get cookie
     *
     * @param $request Request
     * @param $name string
     *
     * @return string
     */
    public function get( Request $request , $name )
    {
        $cookie = FigRequestCookies::get( $request , $name );
        return isset( $cookie ) ? $cookie->getValue() : NULL;
    }

    /**
     * Set cookie
     *
     * @param Response $response
     * @param string $name
     * @param string $value
     * @param DateTime $expiresAt
     * @param string $path
     * @param string $domain
     * @param bool $secure
     * @param bool $httponly
     *
     * @return Response
     */
    public function set( Response $response , $name , $value , $expiresAt = null , $path = null , $domain = null , $secure = false , $httponly = true )
    {
        return FigResponseCookies::set( $response , SetCookie::create( $name , $value )
            ->rememberForever()
            ->withExpires( $expiresAt )
            ->withPath( $path )
            ->withDomain( $domain )
            ->withSecure( $secure )
            ->withHttpOnly( $httponly )
        );
    }

    /**
     * delete cookie
     *
     * @param Response $response
     * @param string $name
     *
     * @return Response
     */
    public function delete( Response $response , $name )
    {
        return FigResponseCookies::expire( $response , $name );
    }


}


