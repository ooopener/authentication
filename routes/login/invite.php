<?php

$application->get
(
    '/login/invite' , [ $container->inviteController , 'get'   ]
)
->setName('api.login.invite') ;

$application->post
(
    '/users/invite' , [ $container->inviteController , 'post'   ]
)
->setName('api.users.invite.post') ;

