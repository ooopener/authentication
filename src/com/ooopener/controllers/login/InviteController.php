<?php

namespace com\ooopener\controllers\login;

use com\ooopener\validations\InviteValidator;
use Psr\Http\Message\ServerRequestInterface as Request ;
use Psr\Http\Message\ResponseInterface as Response ;

use Exception ;

use com\ooopener\controllers\Controller ;

use Slim\Container;

/**
 * The invite controller.
 */
class InviteController extends Controller
{
    /**
     * Creates a new InviteController instance.
     *
     * @param Container $container
     */
    public function __construct( Container $container )
    {
        parent::__construct( $container );
    }

    /**
     * Get invite
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function get( Request $request = NULL, Response $response = NULL , array $args = [] )
    {

        try
        {
            // handle invitation

            $params = $request->getQueryParams() ;

            if( isset( $params['code'] ) )
            {
                // check invitation code
                $checkInvitation = $this->container->invitationCodes->check( $params['code'] ) ;

                if( $checkInvitation )
                {
                    $_SESSION['invitation'] = $checkInvitation ;

                    if( $checkInvitation->redirect && $checkInvitation->redirect != '' )
                    {
                        $_SESSION['urlRedirect'] = $checkInvitation->redirect ;
                    }

                    return $this->pathFor( $response , 'api.login' ) ;
                }
            }

            return $this->render
            (
                $response ,
                'login/invite.twig'
            ) ;
        }
        catch( Exception $e )
        {
            return $this->formatError( $response ,"500", [ $this . ' get', $e->getMessage() ]  , NULL , 500 );
        }

    }

    /**
     * Post invite
     *
     * @param Request|NULL $request
     * @param Response|NULL $response
     * @param array $args
     *
     * @return mixed
     */
    public function post( Request $request = NULL, Response $response = NULL , array $args = [] )
    {
        $container = $this->container ;

        $params = $request->getParsedBody() ;

        $item = [];


        if( isset( $params['team'] ) )
        {
            $item['team'] = $params['team'] ;
        }

        if( isset( $params['email'] ) )
        {
            $item['email'] = $params['email'] ;
        }

        $conditions =
        [
            'team'    => [ $params['team']    , 'required|team' ] ,
            'email'   => [ $params['email']   , 'required|email|userEmail' ]
        ];

        if( isset( $params['person'] ) && $params['person'] != '' )
        {
            $item['person'] = $params['person'] ;
            $conditions['person'] = [ $params['person']  , 'person' ] ;
        }

        if( isset( $params['redirect'] ) && $params['redirect'] != '' && property_exists( $container->jwt , 'aud' ) )
        {
            $item['redirect'] = $params['redirect'] ;
            $conditions['redirect'] = [ $params['redirect']  , 'redirect' ] ;
        }

        ////// validator

        $validator = new InviteValidator( $this->container ) ;

        $validator->validate( $conditions ) ;

        if( $validator->passes() )
        {
            try
            {
                // get user id
                $item['user'] = $container->jwt->sub ;

                // get user ip
                $item['ip'] = $container->userInfos->getIp() ;

                // generate code
                $item['code'] = $container->hash->generateUUID() ;

                $expire = new \DateTime( "now +" . $this->container->settings['token']['invite_code'] . " seconds" , new \DateTimeZone( 'UTC' ) ) ;

                $item['expired'] = $expire->format( "Y-m-d\TH:i:s.v\Z" ) ;
                $item['used'] = 0 ;

                $result = $container->invitationCodes->insert( $item );

                if( $result )
                {
                    // set name
                    $item['name'] = $container->auth->getFullNameOrUsername() ;

                    // send email
                    $resultSendMail = $container->mailer->sendInvitation( $item ) ;

                    if( $resultSendMail )
                    {
                        return $this->success( $response , 'ok' );
                    }
                    else
                    {
                        $container->invitationCodes->delete( $result->_key ) ;
                    }

                }

                return $this->error( $response , '' ) ;

            }
            catch( Exception $e )
            {
                return $this->formatError( $response ,"500", [ $this . ' post', $e->getMessage() ]  , NULL , 500 );
            }
        }
        else
        {
            $errors = [] ;

            $err  = $validator->errors() ;
            $keys = $err->keys() ;

            foreach( $keys as $key )
            {
                $errors[$key] = $err->first($key) ;
            }

            return $this->error( $response , $errors , "400" ) ;
        }


    }

}


