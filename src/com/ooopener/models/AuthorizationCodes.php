<?php

namespace com\ooopener\models;

use com\ooopener\things\Thing;

use Slim\Container ;

/**
 * This service to manage the authorization_codes table.
 */
class AuthorizationCodes extends Model
{
    /**
     * Creates a new AuthorizationCodes instance.
     *
     * @param Container $container
     * @param string $table
     * @param array $init
     */
    public function __construct( Container $container = NULL , $table = NULL , $init = [] )
    {
        parent::__construct( $container , $table , $init );
    }

    ///////////////////////////

    /**
     * The enumeration of all the fillable fields.
     */
    public $fillable =
    [
        'id'        => Thing::FILTER_INT ,
        'user_id'   => Thing::FILTER_DEFAULT ,
        'client_id' => Thing::FILTER_DEFAULT ,
        'code'      => Thing::FILTER_DEFAULT ,
        'ip'        => Thing::FILTER_DEFAULT
    ];

    /**
     * The enumeration of all the fillable fields.
     */
    public $sortable =
    [
        'id'        => 'CAST(id as SIGNED INTEGER)',

        'created'   => 'created'
    ];

    ///////////////////////////

    public function check( $code )
    {
        $query = 'FOR doc IN ' . $this->table . ' FILTER doc.code == @code && doc.expired > DATE_ISO8601( DATE_NOW() ) RETURN doc' ;

        $this->container->arango->prepare
        ([
            'query'    => $query ,
            'bindVars' => [ "code" => $code ]
        ]);

        $this->container->arango->execute() ;

        return $this->container->arango->getObject() ;
    }

    /**
     * Insert a new item into the table.
     *
     * @param array $init
     *
     * @return NULL|integer
     */
    public function insert( $init )
    {
        if( $init && is_array( $init ) )
        {
            $binds  = [] ;
            $values = [] ;

            foreach( $this->fillable as $property => $filter )
            {
                if( array_key_exists( $property , $init ) )
                {
                    $binds[ $property ] = $init[$property] ;
                    $values[] = $property . ':@' . $property ;
                }
            }

            // add dates
            $values[] = 'created: DATE_ISO8601( DATE_NOW() )' ;
            $values[] = 'expired: DATE_ADD( DATE_NOW() , 30 , "s" )' ;

            $values  = implode(',', $values);

            $query = 'INSERT { ' . $values . ' } IN ' . $this->table ;

            $this->container->arango->prepare
            ([
                'query'    => $query ,
                'bindVars' => $binds
            ]);

            $this->container->arango->execute() ;

            return TRUE ;
        }

        return null ;
    }
}


